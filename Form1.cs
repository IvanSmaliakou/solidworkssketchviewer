﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;

namespace MiAKG_Dop
{
    public partial class Form1 : Form
    {
        private IModelDoc2 swModel;
        private ISldWorks  app;
        int fileerror;
        int filewarning;

        public Form1(String filename)
        {
            InitializeComponent();
            var progId = "SldWorks.Application";

            var progType = Type.GetTypeFromProgID(progId);

            app = Activator.CreateInstance(progType) as ISldWorks;
            app.Visible = true;
            swModel = app.OpenDoc6(filename, (int)swDocumentTypes_e.swDocPART, (int)swOpenDocOptions_e.swOpenDocOptions_Silent, "", ref fileerror, ref filewarning);
            Task.Run(async delegate
            {
                await Task.Delay(500);
                ShowWarn(MessageBoxIcon.Information);
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (swModel.SketchManager.ActiveSketch == null) {
                ShowWarn(MessageBoxIcon.Error);
                return;
            }
            listView1.Clear();
            foreach (var item in swModel.SketchManager.ActiveSketch.GetSketchSegments()) {
                if (item is SketchArc) {
                    SketchArc arc = item as SketchArc;
                    listView1.Items.Add("Arc: [ radius: " + arc.GetRadius() * 1000 + " mm ]");
                }
                if (item is SketchEllipse)
                {
                    SketchEllipse ellipse = item as SketchEllipse;
                    listView1.Items.Add("Ellipse: { major pt: " + ellipse.GetMajorPoint2().X * 1000 + "mm," + ellipse.GetMajorPoint2().Y * 1000 + "mm," + ellipse.GetMajorPoint2().Z * 1000 + "mm }");
                }
                if (item is SketchLine)
                {
                    SketchLine line = item as SketchLine;
                    var startPt = line.GetStartPoint2();
                    var endPt = line.GetEndPoint2();

                    listView1.Items.Add(String.Format("Line:[ Start:[ X:{0:F8}mm, Y: {1:F8}mm, Z: {2:F8}mm ]; End: [X: {3:F8}mm, Y: {4:F8}mm, Z: {5:F8}mm ] ]", startPt.X * 1000.0, startPt.Y * 1000.0 , startPt.Z * 1000.0,
                        endPt.X * 1000.0, endPt.Y * 1000.0, endPt.Z * 1000.0));
                }
                if (item is SketchParabola) {
                    SketchParabola parabola = item as SketchParabola;
                    listView1.Items.Add("Parabola: { apex pt: " + parabola.GetApexPoint2().X+ "mm,"+ parabola.GetApexPoint2().Y+ "mm," + parabola.GetApexPoint2().Z + "mm}");
                }
                if (item is SketchText)
                {
                    SketchText text = item as SketchText;
                    listView1.Items.Add("Text: " + text);
                }
            }
        }
        private void ShowWarn(MessageBoxIcon level) {
            MessageBox.Show("Выберите эскиз в проекте SolidWorks", "Внимание!",
                MessageBoxButtons.OK, level);
        }
    }
}
