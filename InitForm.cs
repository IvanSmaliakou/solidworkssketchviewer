﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiAKG_Dop
{
    public partial class InitForm : Form
    {
        Form1 outputForm;
        public InitForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            String filename = openFileDialog2.FileName;
            if (outputForm == null || outputForm.IsDisposed) {
                outputForm = new Form1(filename);
            }
            outputForm.Show();
        }
    }
}
